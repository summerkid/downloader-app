import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;

public class MyDownloader {

    public static int percent = 0;

    public static void dl(URL url) throws IOException {
        HttpURLConnection conn =
                (HttpURLConnection) url.openConnection();
        String fileName = url.getFile().substring(url.getFile().lastIndexOf('/') + 1);
        int fileSize = conn.getContentLength();
        BufferedInputStream inputStream = new BufferedInputStream(conn.getInputStream());
        FileOutputStream fileOutputStream = new FileOutputStream(fileName);
        BufferedOutputStream bufferedOutputStream = new BufferedOutputStream(fileOutputStream, 1024);
        byte[] data = new byte[1024];
        float totalDataRead = 0;
        int i;
        while ((i = inputStream.read(data, 0, 1024))>=0 ) {
            totalDataRead = totalDataRead + i;
            bufferedOutputStream.write(data, 0, i);
            percent = (int) (totalDataRead * 100) / fileSize;
        }
        percent = 100;
        bufferedOutputStream.close();
        inputStream.close();
    }
}