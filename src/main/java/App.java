import java.io.IOException;
import java.util.Scanner;

public class App {
    public static void main(String[] args) throws IOException, InterruptedException {
        Scanner userInput = new Scanner(System.in);
        System.out.println("Please provide an URL for download");
        final String url = userInput.nextLine();
        Thread t = new Thread() {
            public void run() {
                try {
                    MyDownloader.dl(new java.net.URL(url));
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        };
        t.start();
        String anim = "|/-\\";
        do {
            Thread.sleep(100);
            String data = "\r" + anim.charAt(MyDownloader.percent % anim.length())  + " " + MyDownloader.percent;
            System.out.write(data.getBytes());
        } while (MyDownloader.percent < 100);
    }
}